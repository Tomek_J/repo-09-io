package pl.io;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.io.IOException;

public class WritingService {

	public void writeBytes() throws IOException //zapis liczb w postaci bajtow - dla liczb >255 zapisywany jest tylko najmlodszy bajt 
	{
		int[] tab = {0,1,2,3,4,5,256,300};
		
		OutputStream out = new FileOutputStream("c:/Users/Tomek/OneDrive/Dokumenty/UCZELNIA/Aplikacje Rzeczy Internetu/I semestr/Java/byte.bin");
		
		for(int v: tab){
			
			out.write(v);	
		}
		out.close();
	}
	
	public void writeBinaryData() throws IOException //zapis liczb gdzie okreslamy ich typ, wiec program wie na ilu bajtach ja zapisac
	{
		int[] tab = {0,1,2,3,4,5,256,300};
		
		OutputStream out = new FileOutputStream("c:/Users/Tomek/OneDrive/Dokumenty/UCZELNIA/Aplikacje Rzeczy Internetu/I semestr/Java/int.bin");
		DataOutputStream dataOut = new DataOutputStream(out);
		
		for(int v : tab){
			dataOut.writeInt(v);
		}
		dataOut.close();
		
		double[] doubleTab = {3, 45.5,98.989777, -987.011};
		out = new FileOutputStream("c:/Users/Tomek/OneDrive/Dokumenty/UCZELNIA/Aplikacje Rzeczy Internetu/I semestr/Java/double.bin");
		dataOut = new DataOutputStream(out);
		
		for(double v : doubleTab){
			dataOut.writeDouble(v);
		}
		dataOut.close();
	}
	
	public void writeText() throws IOException //zapis tekstu na najnizszym poziomie
	{
		String text = "Ala ma kota";
		int a = 5;
		double b = 45.876;
		
		
		OutputStreamWriter out = new FileWriter("c:/Users/Tomek/OneDrive/Dokumenty/UCZELNIA/Aplikacje Rzeczy Internetu/I semestr/Java/string.txt");
		out.write(text);
		out.write("\r\n"); //powrot karetki i newline
		out.write(String.valueOf(a));
		out.write("\r\n");
		out.write(String.valueOf(b));
		out.close();
	}
	
	public void writeFormattedText() throws IOException //zapis tekstu gdzie mozna uzywac np println
	{
		Writer out = new FileWriter("c:/Users/Tomek/OneDrive/Dokumenty/UCZELNIA/Aplikacje Rzeczy Internetu/I semestr/Java/formatted.txt");
		PrintWriter printOut = new PrintWriter(out);
		
		printOut.println("Ala ma kota");
		printOut.println(5);
		printOut.println(45.876);
		
		String name  = "Ola";
		int age = 45;
		double money = 2567.99;
		
		printOut.format("%s ma %d lat i zarabia %f z�.", name,age,money);
		
		printOut.close();
	}
}
