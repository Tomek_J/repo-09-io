package pl.io;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;

public class ReadingService {
	
	public void readBytes() throws IOException //odczyt z pliku bajt po bajcie
	{
		InputStream in = new FileInputStream("c:/Users/Tomek/OneDrive/Dokumenty/UCZELNIA/Aplikacje Rzeczy Internetu/I semestr/Java/byte.bin");
		
		int data = in.read(); //do odczytu bajtow domyslnie uzywamy int (mimo ze jest 4 bajtowy to tutaj bedzie 1 bajtowy)
		while(data != -1)
		{
			System.out.println(data);
			data = in.read();// dla liczb >255 mamy tylko (liczba - 256)
		}
		in.close();
	}

	public void readBytes2() throws IOException //odczyt pliku wiekszej liczby bajtow
	{
		File f = new File("c:/Users/Tomek/OneDrive/Dokumenty/UCZELNIA/Aplikacje Rzeczy Internetu/I semestr/Java/byte.bin");
		InputStream in = new FileInputStream(f);
		
		long fileLength = f.length(); //dlugosc pliku (wielkosc)
		
		byte[] tab = new byte[(int)fileLength];
		int result = in.read(tab);
		//System.out.println(result);//??
		in.close();
	}
	
	public void readBinaryData() throws IOException //odczyt danych typu int lub double
	{
		InputStream in  = new FileInputStream("c:/Users/Tomek/OneDrive/Dokumenty/UCZELNIA/Aplikacje Rzeczy Internetu/I semestr/Java/int.bin");
		DataInputStream dataIn = new DataInputStream(in);
		
		while(dataIn.available() > 0)
		{
			int data = dataIn.readInt();
			System.out.println(data);
		}
		dataIn.close();
		
		in  = new FileInputStream("c:/Users/Tomek/OneDrive/Dokumenty/UCZELNIA/Aplikacje Rzeczy Internetu/I semestr/Java/double.bin");
		dataIn = new DataInputStream(in);
		
		while(dataIn.available() > 0)
		{
			double data = dataIn.readDouble();
			System.out.println(data);
		}
		dataIn.close();
	}
	
	public void readText() throws IOException //odczyt tekstu na niskim poziomie
	{
		Reader r = new FileReader("c:/Users/Tomek/OneDrive/Dokumenty/UCZELNIA/Aplikacje Rzeczy Internetu/I semestr/Java/formatted.txt");
		
		int data = r.read();
		while(data != -1)
		{
			System.out.print((char)data); //metoda filereader odczytuje w int, rzutujemy do chara, odczyt litera po literze
			data = r.read();
		}
		r.close();
	}
	
	public void readFormattedText() throws IOException
	{
		Reader in = new FileReader("c:/Users/Tomek/OneDrive/Dokumenty/UCZELNIA/Aplikacje Rzeczy Internetu/I semestr/Java/formatted.txt");
		BufferedReader br = new BufferedReader(in); //mozna odczytac tekst jako cala linie i odczyt jest do stringa
		
		String line = br.readLine(); //odczyt pierwszego znaku
		while(line != null) //odczyt tak dlugo poki cos jest w pliku
		{
			System.out.println(line);
			line = br.readLine();
		}
		br.close();
	}
}
