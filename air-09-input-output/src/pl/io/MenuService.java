package pl.io;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;


public class MenuService {

	private OutputStream out;
	private InputStream inScanner;
	private InputStream inData;
	private Scanner scanIn;
	private DataOutputStream dataOut;
	DataInputStream dataIn;
	
	public MenuService(){}
	
	public MenuService(InputStream inScanner, InputStream inData, DataInputStream dataIn, OutputStream out,DataOutputStream dataOut)
	{
		this.inScanner = inScanner;
		this.inData = inData;
		this.dataIn = dataIn;
		this.out = out;
		this.dataOut = dataOut;
	}
	
	public void printCmd()
	{
		System.out.println("Wpisz 1, aby wpisa� liczby.");
		System.out.println("Wpisz 2, aby wy�wietli� wprowadzone liczby");
		System.out.println("Wpisz exit, aby zako�czy�");
	}
	
	public void run() throws IOException
	{
		printCmd();
		dataIn = new DataInputStream(inData);
		scanIn = new Scanner(inScanner);
		scanIn.useLocale(Locale.UK);
		boolean loop = true;
		
		while(loop)
		{
			String choice = scanIn.next();
			switch(choice)
			{
			case "1":
				System.out.println("Wprowadz litere, aby zakonczyc wprowadzanie liczb");
				System.out.println("Wprowadz liczby:");
				writingDouble();
				break;
			case "2":
				System.out.println("Wprowadzone liczby: ");
				readingDouble();
				break;
			case "exit":
				loop = false;
				System.out.println("bye");
				scanIn.close();
				break;
			}
				
		}


	}
	public void writingDouble() throws IOException
	{
		double number=0;
		List<Double> doubleList = new ArrayList<>();

		boolean loop2=true;
		while(loop2)
		{
		if(scanIn.hasNextDouble())
		{
			number = scanIn.nextDouble();
			doubleList.add(number);
		}else
			loop2=false;
		}
		for(double a : doubleList)
		{
			dataOut.writeDouble(a);
		}

		dataOut.close();
		//wyswietlanie listy doublelist
		/*for (double b : doubleList){
			System.out.println(b);
		}*/
	}
	
	public void readingDouble() throws IOException
	{			
		while(dataIn.available() > 0)
		{
			double data = dataIn.readDouble();
			System.out.println(data);
		}
		dataIn.close();
	}
}
